package pe.com.michaelpage.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

@Data
public class Usuario implements Serializable {

    private String id;
    private String name;
    private String email;
    private String password;
    private ArrayList<Phones> listPhones;
    private boolean isactive;

}
package pe.com.michaelpage.model;

import lombok.Data;

@Data
public class Phones {

    private String number;
    private String citycode;
    private String countrycode;
    private Usuario user;

}

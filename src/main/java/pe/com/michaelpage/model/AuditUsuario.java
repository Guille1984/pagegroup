package pe.com.michaelpage.model;

import lombok.Data;

import java.util.Date;

@Data
public class AuditUsuario {

    private String id;
    private Date created;
    private Date modified;
    private Date last_login;

}
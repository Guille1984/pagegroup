package pe.com.michaelpage.controller;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pe.com.michaelpage.controller.commons.ObjectMessage;
import pe.com.michaelpage.controller.commons.ObjectResponse;
import pe.com.michaelpage.controller.enums.CRUDENUM;
import pe.com.michaelpage.model.Usuario;
import pe.com.michaelpage.service.IUsuarioService;
import pe.com.michaelpage.controller.generic.GenericController;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/usuarios")
public class UsuarioController extends GenericController {

    @Autowired
    public IUsuarioService service;

    @PostMapping
    public ResponseEntity<ObjectResponse> save(@RequestBody @Validated Usuario usuario, BindingResult result) {

        if (result.hasErrors()) {
            return super.badRequest(result);
        }

        try {
            Usuario restCliente = service.guardar(usuario);
            if (restCliente != null) {
                return super.ok(restCliente, CRUDENUM.REGISTRO);
            }
            return super.badRequest("Eror al registrar el usuario");
        } catch (ServiceException e) {
            log.error(e.getMessage(), e);
            return super.customError("Eror al registrar el usuario");
        }
    }

    @GetMapping
    public ResponseEntity<ObjectResponse> findAll() {
        try {

            List<Usuario> listaUsuarios = service.listaUsuarios();
            if (listaUsuarios.isEmpty()) {
                 return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                 ObjectResponse.builder()
                 .message(ObjectMessage.builder().code(0).message("No existen registros").build
                 ()) .data(null) .build() );
            }
            return super.ok(listaUsuarios, CRUDENUM.CONSULTA);
        } catch (ServiceException e) {
            log.error(e.getMessage(), e);
            return super.error(e);
        }
    }
}
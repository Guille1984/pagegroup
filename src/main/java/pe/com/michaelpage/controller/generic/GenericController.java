package pe.com.michaelpage.controller.generic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import pe.com.michaelpage.controller.commons.ObjectMessage;
import pe.com.michaelpage.controller.commons.ObjectResponse;
import pe.com.michaelpage.controller.enums.CRUDENUM;

public class GenericController {

	protected List<Map<String, String>> getErrrors(BindingResult result) {

		List<Map<String, String>> errors = result.getFieldErrors().stream().map(err -> {
			Map<String, String> error = new HashMap<>();
			error.put(err.getField(), err.getDefaultMessage());
			return error;
		}

		).collect(Collectors.toList());
		return errors;
	}

	protected ResponseEntity<ObjectResponse> notFound() {
		return customNotFound(null);
	}

	protected ResponseEntity<ObjectResponse> notFound(Long id) {
		return customNotFound(id);
	}

	protected ResponseEntity<ObjectResponse> badRequest(Object msg) {

		if (msg instanceof BindingResult) {
			msg = this.getErrrors((BindingResult) msg);
		}

		return ResponseEntity.badRequest().body(ObjectResponse.builder()
				.message(ObjectMessage.builder().code(-1).message(msg).build()).data(null).build());
	}

	@SuppressWarnings("incomplete-switch")
	protected ResponseEntity<ObjectResponse> ok(Object obj, CRUDENUM crud) {

		if (crud == CRUDENUM.REGISTRO) {
			return this.created(obj);
		} else {

			String msg = "";

			switch (crud) {
			case CONSULTA:
				msg = "Exito de consulta";
				break;
			case ACTUALIZACION:
				msg = "Exito de actualización";
				break;
			case ELIMINACION:
				msg = "Exito de eliminación";
				break;

			}
			return ResponseEntity.ok(ObjectResponse.builder()
					.message(ObjectMessage.builder().code(1).message(msg).build()).data(obj).build());
		}
	}

	protected ResponseEntity<ObjectResponse> error(Exception e) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

	protected ResponseEntity<ObjectResponse> error() {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
	}

	public ResponseEntity<ObjectResponse> customError(Object msg) {
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ObjectResponse.builder()
				.message(ObjectMessage.builder().code(1).message("Error interno").build()).data(null).build());
	}

	private ResponseEntity<ObjectResponse> created(Object resCliente) {

		return ResponseEntity.status(HttpStatus.CREATED)
				.body(ObjectResponse.builder()
						.message(ObjectMessage.builder().code(1).message("Exito de registro").build()).data(resCliente)
						.build());

	}

	private ResponseEntity<ObjectResponse> customNotFound(Long id) {
		String msg = (id != null) ? "No existe resultado con el Id= " + id + " especificado" : "No existe registros";
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ObjectResponse.builder()
				.message(ObjectMessage.builder().code(0).message(msg).build()).data(null).build());
	}

}

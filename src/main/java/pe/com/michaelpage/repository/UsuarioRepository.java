package pe.com.michaelpage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.michaelpage.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario,String> {
}

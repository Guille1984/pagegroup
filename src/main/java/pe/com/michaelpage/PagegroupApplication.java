package pe.com.michaelpage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PagegroupApplication {

    public static void main(String[] args) {
        SpringApplication.run(PagegroupApplication.class, args);
    }

}

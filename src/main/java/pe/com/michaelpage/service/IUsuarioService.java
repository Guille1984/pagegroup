package pe.com.michaelpage.service;

import pe.com.michaelpage.model.Usuario;

import java.util.ArrayList;

public interface IUsuarioService {

    public Usuario guardar(Usuario usuario);
    public Usuario actualizar(Usuario usuario);
    public void eliminar(String id);
    public ArrayList<Usuario> listaUsuarios();
    public Usuario buscarbyId(String id);
}
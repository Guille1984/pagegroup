package pe.com.michaelpage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.michaelpage.model.Usuario;
import pe.com.michaelpage.repository.UsuarioRepository;
import java.util.ArrayList;

@Service
public class UsuarioServiceImpl implements IUsuarioService{

    @Autowired
    public UsuarioRepository repository;

    @Override
    public Usuario guardar(Usuario usuario) {
        return repository.save(usuario);
    }

    @Override
    public Usuario actualizar(Usuario usuario) {
        return repository.save(usuario);
    }

    @Override
    public void eliminar(String id) {
        repository.deleteById(id);
    }

    @Override
    public ArrayList<Usuario> listaUsuarios() {
        return (ArrayList<Usuario>) repository.findAll();
    }

    @Override
    public Usuario buscarbyId(String id) {
        return repository.getById(id);
    }
}
